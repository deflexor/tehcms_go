PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "categories" ("Id" integer not null primary key autoincrement, "Title" varchar(255) not null, "Path" varchar(255), "Description" varchar(255));
INSERT INTO "categories" VALUES(1,'test','0001','');
INSERT INTO "categories" VALUES(2,'бытовая техника','0002','');
INSERT INTO "categories" VALUES(3,'холодильники','00020003','');
INSERT INTO "categories" VALUES(4,'телевизоры','00020004','');
CREATE TABLE "products" ("Id" integer not null primary key autoincrement, "Title" varchar(255) not null, "Category" integer not null, "Description" varchar(255), "Price" integer, "Price1" integer, "Price2" integer, "Imagestr" varchar(255));
INSERT INTO "products" VALUES(1,'Орск',3,'хороший холодильник',100,0,0,'');
INSERT INTO "products" VALUES(2,'Pozis',3,'КОРОТКО О ТОВАРЕ

холодильник
60x63x162 см
двухкамерный
класс A+
морозильник снизу
общий объем 285 л
капельная система разморозки',150,0,0,'389071953.jpg');
CREATE TABLE "category_attrs" ("Attr" varchar(255) not null, "Category" integer not null, "SearchType" varchar(255), primary key ("Attr", "Category"));
INSERT INTO "category_attrs" VALUES('цвет',3,'text');
INSERT INTO "category_attrs" VALUES('авторазморозка',3,'checkbox');
INSERT INTO "category_attrs" VALUES('пульт ДУ',4,'select');
CREATE TABLE "product_attrvals" ("Attr" varchar(255) not null, "Product" integer not null, "Value" varchar(255), primary key ("Attr", "Product"));
INSERT INTO "product_attrvals" VALUES('авторазморозка',1,'1');
CREATE TABLE "articles" ("Id" integer not null primary key autoincrement, "Title" varchar(255), "Added" integer, "Text" varchar(255), "Tagstr" varchar(255));
CREATE TABLE "article_tags" ("Name" varchar(255) not null primary key);
CREATE TABLE "users" ("Id" integer not null primary key autoincrement, "Login" varchar(255) not null unique, "Email" varchar(255) unique, "Name" varchar(255) not null, "Password" varchar(255), "Groupstr" varchar(255), "Status" varchar(255), "Added" integer);
INSERT INTO "users" VALUES(1,'admin','','','$2a$05$0NV6rbFhEmoc7bY04kjnTOMxIJvqGhahFpQ1WZjIkrh51n0Y.DNu2','admins
','confirmed',0);
CREATE TABLE "user_groups" ("Name" varchar(255) not null primary key);
INSERT INTO "user_groups" VALUES('admins');
INSERT INTO "user_groups" VALUES('users');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('categories',4);
INSERT INTO "sqlite_sequence" VALUES('users',1);
INSERT INTO "sqlite_sequence" VALUES('products',2);
COMMIT;
