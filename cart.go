package main

import (
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type Cart struct {
	Items []CartItem
}

type CartItem struct {
	Product Product
	Qty     int
}

func (c Cart) Qty() int {
	return len(c.Items)
}

func (c Cart) Total() int64 {
	var sum int64
	for i, _ := range c.Items {
		sum += c.Items[i].Product.Price * int64(c.Items[i].Qty)
	}
	return sum
}

//
// Controllers
//

func cartPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	cookie, _ := r.Cookie("techcmsYourCart")
	data := PageData{}
	data.Categories, _ = fetchAllCategories()
	if cookie != nil {
		log.Printf("!!! %+v\n", cookie.Value)
		cart, err1 := parseCartCookie(url.QueryUnescape(cookie.Value))
		if !notifyError(w, err1) {
			data.Cart = cart
		}
	}
	execTemplate("cart", w, data)
}

func parseCartCookie(cookie string, err error) (*Cart, error) {
	//if cookie == "null" {
	tokens := strings.FieldsFunc(cookie, func(n rune) bool { return n == ':' })
	if err != nil || len(tokens) == 0 {
		return &Cart{}, nil
	}
	items := make([]CartItem, 0, len(tokens)-1)
	//cookieMap["price"] = tokens[0]
	for i := 0; i < len(tokens); i++ {
		var prod *Product
		kv := strings.FieldsFunc(tokens[i], func(n rune) bool { return n == '-' })
		id, err := strconv.ParseInt(kv[0], 10, 64)
		qty, _ := strconv.Atoi(kv[2])
		if err == nil {
			prod, err = fetchProductById(id)
		}
		if err != nil {
			return nil, err
		}
		items = append(items, CartItem{*prod, qty})
	}
	return &Cart{items}, nil
}
