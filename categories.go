package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"net/http"
	"strconv"
)

const PATH_PART_LEN = 4

type Category struct {
	Id          int64          `json:"id,string"`
	Title       string         `json:"title"`
	Path        string         `json:"path"`
	Description string         `json:"description"`
	Parent      string         `json:"parent" db:"-"`
	Attrs       []CategoryAttr `json:"attrs" db:"-"`
}

type CategoryAttr struct {
	Attr       string `json:"attr"`
	Category   int64  `json:"category,string"`
	SearchType string `json:"searchType"`
}

func (c *Category) Depth() int {
	return len(c.Path) / PATH_PART_LEN
}

func (c *Category) FullPath() string {
	return c.Path + idToPath(c.Id)
}

func (c *Category) Validate() ValidationResult {
	var r ValidationResult
	if c.Title == "" {
		r.Errors = append(r.Errors, ValidationError{"title", "empty"})
	}
	for i, attr := range c.Attrs {
		if attr.Attr == "" {
			r.Errors = append(r.Errors, ValidationError{"attrs[" + strconv.Itoa(i) + "].attr", "empty"})
		}
		if attr.SearchType == "" {
			r.Errors = append(r.Errors, ValidationError{"attrs[" + strconv.Itoa(i) + "].searchType", "empty"})
		}
	}
	return r
}

func fetchAllCategories() ([]*Category, error) {
	var cats []*Category
	var catAttrs []*CategoryAttr
	catById := make(map[int64]*Category)
	_, err1 := Dbh.Select(&cats, "SELECT * FROM categories ORDER BY Path, Title")
	_, err2 := Dbh.Select(&catAttrs, "SELECT * FROM category_attrs")
	if err1 != nil {
		return nil, err1
	}
	if err2 != nil {
		return nil, err2
	}
	for _, cat := range cats {
		cat.Parent = findParentFromPath(cat.Path)
		catById[cat.Id] = cat
	}
	for _, ca := range catAttrs {
		if ca.Category > 0 {
			cat := catById[ca.Category]
			if cat != nil {
				cat.Attrs = append(cat.Attrs, *ca)
			}
		}
	}
	return cats, nil
}

//
// Controllers
//

func apiCategories(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	cats, err := fetchAllCategories()
	if !notifyError(w, err) {
		genericJsonResponse(w, cats)
	}
}

func apiSaveCategory(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	trans, err := Dbh.Begin()
	if notifyError(w, err) {
		return
	}
	defer trans.Commit()
	cat := Category{}
	err = genericValidateJsonRequest(w, r, &cat)
	if err == nil {
		var err error
		if cat.Id > 0 {
			origCat := Category{}
			err = Dbh.SelectOne(&origCat, "SELECT * FROM categories WHERE Id=?", cat.Id)
			if err == nil {
				cat.Path = cat.Parent + idToPath(cat.Id)
				_, err = trans.Update(&cat)
				// If parent is changed then update all cat's children to set new path
				if err == nil && cat.Path != origCat.Path {
					trans.Exec("UPDATE categories SET Path = REPLACE(Path, ?, ?) WHERE Path LIKE ?", origCat.Path, cat.Path, origCat.Path+"%")
				}
			}
		} else {
			err = trans.Insert(&cat)
			if err == nil {
				cat.Path = cat.Parent + idToPath(cat.Id)
				_, err = trans.Update(&cat)
			}
		}
		if notifyError(w, err) {
			return
		}
		// save attrs
		trans.Exec("DELETE FROM category_attrs WHERE Category=?", cat.Id)
		for _, attr := range cat.Attrs {
			attr.Category = cat.Id
			err = trans.Insert(&attr)
			if notifyError(w, err) {
				return
			}
		}
		w.Write([]byte(`{"ok":1}`))
	}
}

func apiDeleteCategory(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	trans, err := Dbh.Begin()
	if notifyError(w, err) {
		return
	}
	defer trans.Commit()
	id, err := strconv.ParseInt(ps.ByName("id"), 10, 64)
	if !notifyError(w, err) {
		cat := Category{}
		err = trans.SelectOne(&cat, "SELECT * FROM categories WHERE Id=?", id)
		if !notifyError(w, err) {
			_, err := trans.Delete(&cat)
			// Update all cat's children to remove it from their path
			if err == nil {
				subPath := idToPath(id)
				_, err = trans.Exec("UPDATE categories SET Path = REPLACE(Path, ?, ?) WHERE Path LIKE ?", subPath, "", cat.Path+"%")
			}
			// Remove also Attrs
			if err == nil {
				_, err = trans.Exec("DELETE FROM category_attrs WHERE Category=?", id)
			}
			if !notifyError(w, err) {
				w.Write([]byte(`{"ok":1}`))
			}
		}
	}
}

func findParentFromPath(path string) string {
	var parent string
	if len(path) >= PATH_PART_LEN*2 {
		parent = path[:len(path)-PATH_PART_LEN]
	}
	return parent
}

func idToPath(id int64) string {
	if id == 0 {
		return ""
	}
	path := strconv.FormatInt(id, 36)
	return fmt.Sprintf("%04s", path)
}
